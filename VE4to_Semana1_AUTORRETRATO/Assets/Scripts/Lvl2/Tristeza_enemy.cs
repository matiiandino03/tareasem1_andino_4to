using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Tristeza_enemy : MonoBehaviour
{
    public GameObject TristezaEnemyprefab;
    public bool isOnLight = false;

    float lightLimit = 0;

    public Image fill;
    public Image fillBackground;
    public GameObject alerta;

    private Camera cam;

    NavMeshAgent navMeshAgent;
    bool isAttacking = false;

    Transform player;

    public CanvasController cc;


    void Start()
    {
        //StartCoroutine("Grow");
        cam = Camera.main;
        navMeshAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Felicidad").transform;        
    }

    void Update()
    {
        if(cc.gameStarted)
        {
            Refresh();
        }

        fill.transform.rotation = Quaternion.LookRotation(fill.transform.position - cam.transform.position);
        fillBackground.transform.rotation = Quaternion.LookRotation(fillBackground.transform.position - cam.transform.position);
        alerta.transform.rotation = Quaternion.LookRotation(alerta.transform.position - cam.transform.position);
        if (transform.localScale.x >= 2)
        {
            Instantiate(TristezaEnemyprefab, new Vector3(transform.position.x + 1, transform.position.y, transform.position.z), Quaternion.identity);
            Instantiate(TristezaEnemyprefab, new Vector3(transform.position.x - 1, transform.position.y, transform.position.z), Quaternion.identity);
            Destroy(this.gameObject);
        }
        if(isOnLight)
        {
            lightLimit += Time.deltaTime;
        }
        if(!isOnLight && lightLimit > 0)
        {
            lightLimit -= (Time.deltaTime/2);
        }
        if(lightLimit < 0)
        {
            lightLimit = 0;
        }
        if(lightLimit > 1)
        {
            Destroy(this.gameObject);
            cc.BajarTristeza();
        }

        if(lightLimit != 0)
        {
            fill.fillAmount = lightLimit;
        }
        else
        {
            fill.fillAmount = 0;
        }
    }

    public void Refresh()
    {
        if(Vector3.Distance(transform.position,player.position) < 15)
        {
            navMeshAgent.SetDestination(player.position);
            alerta.SetActive(true);
            isAttacking = true;
        }
        if(Vector3.Distance(transform.position, player.position) > 15)
        {
            alerta.SetActive(false);
            isAttacking = false;
        }
        if(Vector3.Distance(transform.position, player.position) < 2.5f)
        {
            navMeshAgent.SetDestination(transform.position);
        }
        if(navMeshAgent.velocity == Vector3.zero && !isAttacking)
        {
            int x = Random.Range(-68, 52);
            int z = Random.Range(-46,105);
            navMeshAgent.SetDestination(new Vector3(x,1.43f,z));
        }
    }

    IEnumerator Grow()
    {
        yield return new WaitForSeconds(0.1f);
        gameObject.transform.localScale += new Vector3(0.015f, 0.015f, 0.015f);
        StartCoroutine("Grow");
    }
}
