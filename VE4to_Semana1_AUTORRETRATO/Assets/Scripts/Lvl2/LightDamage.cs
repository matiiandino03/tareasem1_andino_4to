using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightDamage : MonoBehaviour
{
    public PlayerMovement pm;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Tristeza_Enemy") && this.gameObject.activeInHierarchy && pm.isOnLight == true)
        {
            other.GetComponent<Tristeza_enemy>().isOnLight = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Tristeza_Enemy") && this.gameObject.activeInHierarchy)
        {
            other.GetComponent<Tristeza_enemy>().isOnLight = false;
        }
    }
}
