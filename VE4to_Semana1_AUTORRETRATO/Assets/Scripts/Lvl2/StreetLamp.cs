using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetLamp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Felicidad"))
        {
            other.gameObject.GetComponent<PlayerMovement>().isOnLight = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Felicidad"))
        {
            other.gameObject.GetComponent<PlayerMovement>().isOnLight = false;
        }
    }
}
