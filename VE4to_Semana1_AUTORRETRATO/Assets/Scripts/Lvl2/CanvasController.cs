using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CanvasController : MonoBehaviour
{
    public Image tristezaAmmount;
    public Image felicidadAmmount;

    public GameObject DirectionalLight;

    public Image MindSpace;
    public GameObject emociones;
    public GameObject panelWin;

    public int index = 0;

    public bool gameStarted = false;

    public PlayerMovement pm;
    public void BajarTristeza()
    {
        index += 1;
        tristezaAmmount.fillAmount -= (12f/100);
        StartCoroutine("AllOfTheLights");
        StartCoroutine("StopAfterTime");
        felicidadAmmount.fillAmount += (8f / 100);
        if(index == 6)
        {
            panelWin.SetActive(true);
            pm.enabled = false;
        }
    }

    IEnumerator AllOfTheLights()
    {
        yield return new WaitForSeconds(0.1f);
        DirectionalLight.GetComponent<Light>().intensity += 0.01f;
        StartCoroutine("AllOfTheLights");       
    }

    IEnumerator StopAfterTime()
    {
        yield return new WaitForSeconds(3f);
        StopCoroutine("AllOfTheLights");
    }

    public void StartGame()
    {
        MindSpace.CrossFadeAlpha(0, 3, true);
        emociones.SetActive(true);
        gameStarted = true;
    }
    public void EndGame()
    {
        SceneManager.LoadScene("Menu");
    }
}
