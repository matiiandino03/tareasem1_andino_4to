using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private LayerMask groundMask;

    private Camera mainCamera;
    Animator animator;
    NavMeshAgent navMeshAgent;

    public GameObject point;
    public float speed;
    public bool isOnLight;

    public GameObject lights;
    private void Start()
    {
        mainCamera = Camera.main;
        animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        Movement();
        if(navMeshAgent.velocity == Vector3.zero)
        {
            Aim();
            animator.SetBool("IsWalking", false);
        }
        else
        {
            animator.SetBool("IsWalking", true);
        }

        if(isOnLight)
        {
            lights.SetActive(true);
        }
        if(!isOnLight)
        {
            lights.SetActive(false);
        }
    }

    public void Movement()
    {
        navMeshAgent.speed = speed;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if(navMeshAgent.velocity != Vector3.zero)
        {
            transform.eulerAngles = new Vector3(0, Quaternion.LookRotation(navMeshAgent.velocity - Vector3.zero).eulerAngles.y, 0);
        }

        if(Input.GetMouseButtonDown(1))
        {
            if(Physics.Raycast(ray,out RaycastHit raycastHit,float.MaxValue,groundMask))
            {
                point.transform.position = raycastHit.point;
                navMeshAgent.SetDestination(point.transform.position);
            }
        }
    }
    private void Aim()
    {
        var (success, position) = GetMousePosition();
        if (success)
        {
            var direction = position - transform.position;

            direction.y = 0;

            transform.forward = direction;
        }
    }

    private (bool success, Vector3 position) GetMousePosition()
    {
        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hitInfo, Mathf.Infinity, groundMask))
        {
            return (success: true, position: hitInfo.point);
        }
        else
        {
            return (success: false, position: Vector3.zero);
        }
    }
}
