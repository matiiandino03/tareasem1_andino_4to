using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note_Detection : MonoBehaviour
{
    public bool isPressed = false;
    public bool hit = false;

    public Level1_Logic lvl1Logic;
    private void OnTriggerEnter(Collider other)
    {
        if(isPressed)
        {
            Destroy(other.gameObject);
            lvl1Logic.DownGradeLevels(1f);
            hit = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (isPressed)
        {
            Destroy(other.gameObject);
            lvl1Logic.DownGradeLevels(2f);
            hit = true;
        }
    }
}
