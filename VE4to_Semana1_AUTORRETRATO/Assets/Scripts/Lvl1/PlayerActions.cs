using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    public GameObject A;
    public GameObject S;
    public GameObject D;

    public Material matA;
    public Material matAGlow;
    public Material matS;
    public Material matSGlow;
    public Material matD;
    public Material matDGlow;

    public Level1_Logic lvl1Logic;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            A.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            A.GetComponent<Renderer>().material = matAGlow;
            A.GetComponent<Note_Detection>().isPressed = true;
            StartCoroutine("Apressed");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            S.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            S.GetComponent<Renderer>().material = matSGlow;
            S.GetComponent<Note_Detection>().isPressed = true;
            StartCoroutine("Spressed");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            D.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            D.GetComponent<Renderer>().material = matDGlow;
            D.GetComponent<Note_Detection>().isPressed = true;
            StartCoroutine("Dpressed");
        }
    }

    IEnumerator Apressed()
    {
        yield return new WaitForSeconds(0.15f);
        A.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        A.GetComponent<Renderer>().material = matA;
        A.GetComponent<Note_Detection>().isPressed = false;
        if (A.GetComponent<Note_Detection>().hit == false)
        {
            lvl1Logic.EstresAmmount.fillAmount += (8f/100);
        }
        A.GetComponent<Note_Detection>().hit = false;
    }
    IEnumerator Spressed()
    {
        yield return new WaitForSeconds(0.15f);
        S.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        S.GetComponent<Renderer>().material = matS;
        S.GetComponent<Note_Detection>().isPressed = false;
        if (S.GetComponent<Note_Detection>().hit == false)
        {
            lvl1Logic.EstresAmmount.fillAmount += (8f / 100);
        }
        S.GetComponent<Note_Detection>().hit = false;
    }
    IEnumerator Dpressed()
    {
        yield return new WaitForSeconds(0.15f);
        D.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        D.GetComponent<Renderer>().material = matD;
        D.GetComponent<Note_Detection>().isPressed = false;
        if (D.GetComponent<Note_Detection>().hit == false)
        {
            lvl1Logic.EstresAmmount.fillAmount += (8f / 100);
        }
        D.GetComponent<Note_Detection>().hit = false;
    }
}
