using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine("DestroyOverTime");
    }
    private void FixedUpdate()
    {
        transform.Translate(new Vector3(0, 0, -0.1f), Space.Self);
    }

    IEnumerator DestroyOverTime()
    {
        yield return new WaitForSeconds(6);
        Destroy(this.gameObject);
    }
}
