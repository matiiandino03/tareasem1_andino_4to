using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level1_Logic : MonoBehaviour
{
    public float timer = 0f;

    public List<float> TimerNotasA = new List<float>();
    public List<float> TimerNotasS = new List<float>();
    public List<float> TimerNotasD = new List<float>();

    public GameObject A_Note;
    public Transform spawnA;
    public GameObject S_Note;
    public Transform spawnS;
    public GameObject D_Note;
    public Transform spawnD;

    AudioSource audioSource;
    bool isFirst = true;

    public Image EstresAmmount;
    bool restar = false;

    bool gameStarted = false;

    public GameObject emotionsLevels;
    public GameObject panelWin;
    public GameObject panelDerrota;

    public TextAsset txtA;
    public TextAsset txtS;
    public TextAsset txtD;
    private void Awake()
    {
        //Time.timeScale = 0;
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        TimerNotasA = FileHandler.ReadListFromJSON<float>(txtA);
        TimerNotasS = FileHandler.ReadListFromJSON<float>(txtS);
        TimerNotasD = FileHandler.ReadListFromJSON<float>(txtD);
        StartCoroutine("EstresLevelsUp");
    }

    void Update()
    {
        if(gameStarted)
        {
            timer += Time.deltaTime;
            if (timer >= 3.25f && isFirst)
            {
                audioSource.Play();
                isFirst = false;
            }

            if (TimerNotasA.Count != 0 && timer >= TimerNotasA[0])
            {
                Instantiate(A_Note, spawnA.transform.position, spawnA.transform.rotation);
                TimerNotasA.Remove(TimerNotasA[0]);
            }

            if (TimerNotasS.Count != 0 && timer >= TimerNotasS[0])
            {
                Instantiate(S_Note, spawnS.transform.position, spawnS.transform.rotation);
                TimerNotasS.Remove(TimerNotasS[0]);
            }
            if (TimerNotasD.Count != 0 && timer >= TimerNotasD[0])
            {
                Instantiate(D_Note, spawnD.transform.position, spawnD.transform.rotation);
                TimerNotasD.Remove(TimerNotasD[0]);
            }

            if (timer >= 135)
            {
                if(EstresAmmount.fillAmount < 0.3f)
                {
                    panelWin.SetActive(true);
                }
                else
                {
                    panelDerrota.SetActive(true);
                }
            }
        }       

        #region writing json
        //if(Input.GetKeyDown(KeyCode.A))
        //{
        //    A.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        //    TimerNotasA.Add(timer);
        //}
        //if (Input.GetKeyUp(KeyCode.A))
        //{
        //    A.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    S.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        //    TimerNotasS.Add(timer);
        //}
        //if (Input.GetKeyUp(KeyCode.S))
        //{
        //    S.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        //}
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    D.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        //    TimerNotasD.Add(timer);
        //}
        //if (Input.GetKeyUp(KeyCode.D))
        //{
        //    D.transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        //}

        //if(timer >= 150)
        //{
        //    FileHandler.SaveToJSON<float>(TimerNotasA, "Lvl1_A.json");
        //    FileHandler.SaveToJSON<float>(TimerNotasS, "Lvl1_S.json");
        //    FileHandler.SaveToJSON<float>(TimerNotasD, "Lvl1_D.json");
        //}
        #endregion

    }

    public void DownGradeLevels(float rest)
    {
        if(gameStarted)
        {
            EstresAmmount.fillAmount -= (rest / 100);
        }    
    }

    IEnumerator EstresLevelsUp()
    {
        yield return new WaitForSeconds(0.1f);
        bool stop = false;
        bool endGame = false;
        if(timer >= 135)
        {
            endGame = true;
        }
        if (timer > 110)
        {
            EstresAmmount.fillAmount = EstresAmmount.fillAmount + (0.2f / 100);
            stop = true;
        }
        if (EstresAmmount.fillAmount < 0.5f && !stop)
        {
            EstresAmmount.fillAmount = EstresAmmount.fillAmount + (0.6f / 100);
        }
        if(!stop && EstresAmmount.fillAmount >= 0.5f)
        {
            EstresAmmount.fillAmount = EstresAmmount.fillAmount + (0.3f / 100);
        }        
        if(!endGame)
        {
            StartCoroutine("EstresLevelsUp");
        }
    }

    public void StartGame()
    {
        gameStarted = true;
        emotionsLevels.SetActive(true);
    }
    public void EndGame()
    {
        SceneManager.LoadScene("Level2");
    }

    public void Reintentar()
    {
        SceneManager.LoadScene("Level1");
    }

    public void DescativarPanelWin()
    {
        panelWin.SetActive(false);
    }
}

